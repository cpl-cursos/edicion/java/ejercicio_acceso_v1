package utilidades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class conexionBD {
    private static String url = "jdbc:mysql://localhost:3306/acceso";
    private static String username = "root";
    private static String password = "4Cc3s0SQL$";

    public static Connection abrirConn() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }
}
