package filtros;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletResponse;
import utilidades.conexionBD;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebFilter("/*")
/*
        Utilizamos un filtro para establecer una conexión con la BBDD en todas las rutas.
        Con esta conexión se realizarán las consultas a la BBDD.
        En cada petición (request) se ejecuta este filtro. Por tanto, la conexión se establece en todas y cada una de
        las peticiones.
 */
public class conexFiltro implements Filter {
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        // Utilizamos un try con recursos para que la conexión se cierre siempre una vez que se haya utilizado o
        // en caso de error
        try(Connection conex = conexionBD.abrirConn()) {
            // desactivamos el autocommit para utilizar transacciones
            if(conex.getAutoCommit()) {
                conex.setAutoCommit(false);
            }
            try {
                // añadimos la conexión a la petición para que pueda ser utilizada por los siguientes servlets/módulos,
                // o cualquier código que reciba posteriormente el request.
                req.setAttribute("conexion", conex);
                // enviamos el request a la siguiente fase
                chain.doFilter(req, resp);
                // hacemos el commit para volcar a la BBDD
                conex.commit();
            } catch (SQLException e) {
                // Si hay error, deshacemos todas las operaciones.
                conex.rollback();
                ((HttpServletResponse)resp).sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
