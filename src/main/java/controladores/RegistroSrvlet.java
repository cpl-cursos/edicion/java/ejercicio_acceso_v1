package controladores;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/registro")
public class RegistroSrvlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("title", "Registro");
        getServletContext().getRequestDispatcher("/registro.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String usu = req.getParameter("usuario");
        String clave = req.getParameter("clave");
        // Abrir una conexión a la BBD ...
        Connection conex = (Connection) req.getAttribute("conexion");
        // ... y comprobar si existe el usuario
        req.setAttribute("title", "Registro");
        try(PreparedStatement qry = conex.prepareStatement("SELECT * FROM usuarios WHERE emilio = ?")) {
            qry.setString(1, usu);
            try(ResultSet rs = qry.executeQuery()) {
                if(rs.next()) {
                    // si existe , no se puede repetir el emilio ...
                    // enviamos un error
                    req.setAttribute("tipo", "danger");
                    req.setAttribute("mensaje","Este nombre de usuario ya existe. Pruebe con otro.");

                } else {
                    // si NO existe, lo damos de alta en la BBDD
                    try(PreparedStatement qry1 = conex.prepareStatement("INSERT INTO usuarios (emilio, clave) VALUES (?, ?)")) {
                        qry1.setString(1, usu);
                        qry1.setString(2,clave);
                        qry1.executeUpdate();
                        req.setAttribute("tipo", "success");
                        req.setAttribute("mensaje","Usuario creado añadido a la tabla de usuarios..");
                    }
                }
                getServletContext().getRequestDispatcher("/registro.jsp").forward(req, resp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
