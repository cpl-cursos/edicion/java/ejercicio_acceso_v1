package controladores;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import utilidades.conexionBD;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/acceso")
public class LoginSrvlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String usuario = (String) req.getSession().getAttribute("usuario");

        if (usuario != null) {
            /*
                Si la sesión tiene un usuario, es que ya se identificó previamente y vamos a su perfil
             */
            getServletContext().getRequestDispatcher("/perfil.html").forward(req, resp);
        } else {
            /*
               Si la sesión no tiene usuario, fijamos el título de la solapa y pasamos al formulario de login
             */
            req.setAttribute("title", "Acceso");
            getServletContext().getRequestDispatcher("/login.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String usu = req.getParameter("usuario");
        String clave = req.getParameter("clave");
        // preparamos el request a pasar al siguiente...
        req.setAttribute("title", "Acceso");

        // ... Comprobamos si hay un usuaruo ya "logeado"...
        HttpSession sesion = req.getSession();
        // ... si ya está identificado
        if (usu.equals(sesion.getAttribute("usuario"))) {
            req.setAttribute("tipo", "success");
            req.setAttribute("mensaje", "Ya está identificado como " + usu);  // ...avisamos
        } else {    // ... si NO está identificado, le buscamos
            // Abrir una conexión a la BBD ...
            Connection conex = (Connection) req.getAttribute("conexion");
            // ... y comprobar si existe el usuario
            try (PreparedStatement qry = conex.prepareStatement("SELECT * FROM usuarios WHERE emilio = ?")) {
                qry.setString(1, usu);
                try (ResultSet rs = qry.executeQuery()) {
                    if (rs.next() && clave.equals(rs.getString("clave"))) {
                        // si existe y es su clave, añadimos el usuario a la sesión ...
                        sesion.setAttribute("usuario", usu);
                        req.setAttribute("tipo", "success");
                        req.setAttribute("mensaje", "Bienvenido a este sitio web " + usu + ".");
                    } else {
                        // si NO existe, mostramos un error
                        req.setAttribute("tipo", "danger");
                        req.setAttribute("mensaje", "Lo sentimos. No esta autorizado para ingresar a esta página.");
                    }

                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        // ... y volvemos a este mismo servlet, pero con un GET que mostrará el mensaje correspondiente
        getServletContext().getRequestDispatcher("/login.jsp").forward(req, resp);
    }
}
