<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="es_ES">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- Font Icon -->
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Estilos personalizados -->
    <link href="css/custom.css" type="text/css" rel="stylesheet">

    <title>Registro</title>
</head>
<body>
<section class="signup">
    <div class="container">
        <div class="signup-content">
            <div class="signup-form">
                <h2 class="form-title">${titulo}</h2>
                <form action="${pageContext.request.contextPath}/registro" method="POST" class="register-form" id="register-form">
                    <div class="form-group">
                        <label for="nombre"><i class="zmdi zmdi-account material-icons-name"></i></label>
                        <input type="text" name="nombre" id="nombre" placeholder="Su nombre"/>
                    </div>
                    <div class="form-group">
                        <label for="emilio"><i class="zmdi zmdi-email"></i></label>
                        <input type="email" name="emilio" id="emilio" placeholder="Su correo electrónico"/>
                    </div>
                    <div class="form-group">
                        <label for="clave"><i class="zmdi zmdi-lock"></i></label>
                        <input type="password" name="clave" id="clave" placeholder="contraseña"/>
                    </div>
                    <div class="form-group">
                        <label for="re-clave"><i class="zmdi zmdi-lock-outline"></i></label>
                        <input type="password" name="re-clave" id="re-clave" placeholder="Repita la contraseña"/>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="acepta-terminos" id="acepta-terminos" class="agree-term" />
                        <label for="acepta-terminos" class="label-agree-term"><span><span></span></span>Acepto todos los  <a href="#" class="term-service">términos del servicio</a></label>
                    </div>
                    <div class="form-group form-button">
                        <input type="submit" name="registro" id="registro" class="form-submit" value="Registrarme"/>
                    </div>
                </form>
            </div>
            <div class="signup-image">
                <figure><img src="images/signup-image.jpg" alt="sing up image"></figure>
                <a href="${pageContext.request.contextPath}/acceso" class="signup-image-link">Ya estoy resistrado</a>
            </div>
        </div>
    </div>
</section>

<!-- Optional JavaScript; choose one of the two! -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
        crossorigin="anonymous"></script>
</body>
</html>